# GitLab CI Stuff

## Overview

This template provides a jobs to validate your template syntax.

It uses the [GitLab CI Lint API](https://docs.gitlab.com/ee/api/lint.html).

## Template validation

The template implements a `gitlab-ci-lint` job, enabled with `GITLAB_CI_FILES` variable (GitLab CI files pattern(s) to analyse).

Example of a `.gitlab-ci.yml` file for a GitLab CI template project:

```yaml
include:
  - project: 'to-be-continuous/tools/gitlab-ci'
    ref: 'master'
    file: '/templates/validation.yml'

stages:
  - build

variables:
  GITLAB_CI_FILES: "templates/*.yml"
```
